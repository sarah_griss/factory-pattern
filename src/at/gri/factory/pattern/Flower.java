package at.gri.factory.pattern;

public interface Flower {
	public void bloom();
}
