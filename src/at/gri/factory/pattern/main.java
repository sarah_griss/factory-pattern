package at.gri.factory.pattern;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Flower flower = FlowerFactory.getFlower("red");
		flower.bloom();

		flower = FlowerFactory.getFlower("blue");
		flower.bloom();

		flower = FlowerFactory.getFlower("white");
		flower.bloom();

	}

}
