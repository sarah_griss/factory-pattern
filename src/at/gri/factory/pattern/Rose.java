package at.gri.factory.pattern;

public class Rose implements Flower {

	@Override
	public void bloom() {
		// TODO Auto-generated method stub
		System.out.println("The rose smells very good!");
	}

}

class Violet implements Flower {

	@Override
	public void bloom() {
		// TODO Auto-generated method stub
		System.out.println("The violet smells very good!");
	}

}

class Daisy implements Flower {

	@Override
	public void bloom() {
		// TODO Auto-generated method stub
		System.out.println("The daisy smells very good!");
	}

}
