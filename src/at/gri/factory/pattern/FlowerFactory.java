package at.gri.factory.pattern;

public class FlowerFactory {

	public static Flower getFlower(String colour) {
		if (colour.equals("red"))
			return new Rose();
		else if (colour.equals("blue"))
			return new Violet();
		else if (colour.equals("white"))
			return new Daisy();

		return null;
	}

}
